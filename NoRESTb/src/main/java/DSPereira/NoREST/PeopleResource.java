package DSPereira.NoREST;

import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Root resource (exposed at "people" path)
 * REST API for the application.
 */
@Api(value = "/people", description = "Context Root for the application. Operations about People.")
@Path("people")
public class PeopleResource {

	/**
	 * Accepts a Person to introduce in the Repository
	 * 
	 * @param person to be added to the Repository
	 * @return Response with the person added, if it is successfully added. Null otherwise.
	 */
	@POST
	@Path("add")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(httpMethod = "POST", value = "Registers a new Person", response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Person successfully registered!"),
			@ApiResponse(code = 400, message = "Invalid data provided for the Person.") })
	public Response registerPerson(@ApiParam(value = "Person to be registered", required = true)Person person) {

		PeopleRepository repo = PeopleRepository.getInstance();
		Person resultPerson = repo.addPersonToRepository(person);

		if (resultPerson != null) {
			return Response.status(201, "Person successfully registered!").entity(resultPerson).build();
		} else {
			return Response.status(400, "Invalid data provided for the Person.").entity("The data provided is not valid!").build();
		}
	}

	/**
	 * Accepts an id to retrieve a specific person
	 * 
	 * @param id of the person to retrieve.
	 * @return a response with the person associated with the id provided if it is successfully retrieved. Null otherwise.
	 */
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(httpMethod = "GET", value = "Finds the person with the ID provided", response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Person found!"),
			@ApiResponse(code = 404, message = "There is no Person with the ID provided") })
	public Response findSpecificPerson(@ApiParam(value = "ID of the person to be found", required = true) @PathParam("id") String id) {

		PeopleRepository repo = PeopleRepository.getInstance();
		Person foundPerson = repo.findAPersonById(id);

		if (foundPerson != null) {
			return Response.status(200, "Person found!").entity(foundPerson).build();
		} else {
			return Response.status(404, "There is no Person with the ID provided").entity("User not found!").build();
		}
	}

	/**
	 * Updates the person associated with the id provided
	 * 
	 * @param id of the person to be retrieved
	 * @param newPerson with the new data
	 * @return a response with the person if it is successfully updated. Null otherwise.
	 */
	@PATCH
	@Path("/{id}/update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(httpMethod = "PATCH", value = "Updates the person associated to the ID provided", response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Person successfully updated!"),
			@ApiResponse(code = 404, message = "There is no Person with the ID provided")})
	public Response updatePerson(@ApiParam(value = "ID of the person to be updated", required = true) @PathParam("id") String id, 
									@ApiParam(value = "New data to update the Person specified by the ID", required = true) Person newPerson) {

		//gets the person to be updated
		PeopleRepository repo = PeopleRepository.getInstance();
		Person oldPerson = repo.findAPersonById(id);
		
		if(oldPerson == null) {
			return Response.status(404, "The Person you are trying to update does not exist in the Repository.").entity("User not found!")
					.build();
		}
		
		//actually updates the person
		repo.updatePerson(oldPerson, newPerson);

		return Response.status(200, "Person successfully updated!").entity("Update successful!").build();
	}

	/**
	 * Retrieves all the people in the Repository
	 *
	 * @return a response with a map with all the people in it.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(httpMethod = "GET", value = "Returns all the People in the Repository", response = Response.class)
	@ApiResponses(value = @ApiResponse(code = 200, message = "All the People have been retrieved!"))
	public Response getAllPeople() {

		PeopleRepository repo = PeopleRepository.getInstance();
		Map<String, Person> allPeople = repo.retrieveAllPeople();

		return Response.status(200, "All the People have been retrieved!").entity(allPeople).build();

	}

	/**
	 * Deletes the Person with the specified ID
	 * 
	 * @param id of the person to be deleted
	 * @return a response with a status
	 */
	@DELETE
	@Path("{id}/delete")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(httpMethod = "DELETE", value = "Deletes the Person associated to the ID provided", response = Response.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Person successfully deleted!"),
			@ApiResponse(code = 404, message = "The Person you are trying to delete does not exist in the Repository.") })
	public Response deletePerson(@ApiParam(value = "ID of the person to be deleted", required = true) @PathParam("id") String id) {

		PeopleRepository repo = PeopleRepository.getInstance();
		boolean deleteSuccessful = repo.deletePerson(id);

		if (deleteSuccessful == true) {
			return Response.status(200, "Person successfully deleted!").entity("Delete successful!").build();
		} else {
			return Response.status(404, "The Person you are trying to delete does not exist in the Repository.").entity("User not found!")
					.build();
		}
	}
	
}
