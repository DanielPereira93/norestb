/**
 * 
 */
package DSPereira.NoREST;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * @author dspereira
 * 
 * Class that manages the people saved in the application.
 * It also manages the ID generation and attribution.
 *
 */
public class PeopleRepository {

	private int currentId = 0;
	private Map<String, Person> registeredPeople;
	private final static PeopleRepository repo = new PeopleRepository();

	/**
	 * This constructor is made private to avoid misusing this class,
	 * effectively implementing a singleton.
	 */
	private PeopleRepository() {
		this.registeredPeople = new Hashtable<>();
	}

	/**
	 * Method to have access to the People Repository
	 * @return the PeopleRepository singleton
	 */
	public static PeopleRepository getInstance() {

		return repo;

	}

	/**
	 * Returns the current id available, and increments it.
	 * 
	 * @return the id to be set
	 */
	private String generateId() {

		int idToReturn = ++currentId;

		String result = Integer.toString(idToReturn);

		return result;
	}

	/**
	 * Adds a person to the Repository.
	 * 
	 * @param personToAdd to the repository
	 * 
	 * @return the person if it is successfully added, null if something fails or is missing.
	 */

	public Person addPersonToRepository(Person personToAdd) {

		if (personToAdd != null) {
			
			if (isValid(personToAdd.getName()) && isValid(personToAdd.getCountry())) {

				// Adding an ID to the person and adding them to the Repository
				String newId = generateId();

				personToAdd.setId(newId);

				registeredPeople.put(newId, personToAdd);
			} else {
				personToAdd=null;
			}

		}

		return personToAdd;
	}

	/**
	 * Retrieves all the people in the Repository
	 * 
	 * @return the list with all the people
	 */
	public Map<String, Person> retrieveAllPeople() {
		return this.registeredPeople;
	}

	/**
	 * Finds a person based on the id provided. If the id provided is null or not
	 * assigned yet, returns null.
	 * 
	 * @param id of the person to find
	 * @return the person associated with the id provided
	 */
	public Person findAPersonById(String id) {

		Person personToFind = null;

		if (isValid(id)) {
			personToFind = this.registeredPeople.get(id);
		}

		return personToFind;
	}

	/**
	 * Updates a Person based on the data provided.
	 * Only new values which are not null, empty or blank will be updated.
	 * 
	 * @param oldPerson      the person to update
	 * @param newPerson      the person with the new data
	 * 
	 * @return the updated person.
	 */
	public Person updatePerson(Person oldPerson, Person newPerson) {

		if (oldPerson != null && newPerson != null) {

			if (isValid(newPerson.getName())) {
				oldPerson.setName(newPerson.getName());
			}

			if (isValid(newPerson.getCountry())) {
				oldPerson.setCountry(newPerson.getCountry());
			}
		}

		return oldPerson;

	}

	/**
	 * Deletes a Person based on the id provided. If the id provided is null, or if
	 * it is provided but is has yet to be assigned, no Person will be deleted, else
	 * it will delete a Person.
	 * 
	 * @param id of the person to delete
	 * @return true if the person was deleted, false if it wasn't
	 */
	public boolean deletePerson(String id) {

		Person personToDelete = null;

		if (id != null) {
			personToDelete = registeredPeople.remove(id);
		}

		return personToDelete != null;

	}
	
	private boolean isValid(String toCheck) {
		return StringUtils.isNotBlank(toCheck);
	}

}
