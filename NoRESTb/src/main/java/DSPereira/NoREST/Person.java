/**
 * 
 */
package DSPereira.NoREST;

import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author dspereira
 * 
 * Class that represents a person in the application.
 * It has three attributes: the id, the name and the country of the person.
 * All of the attributes are required (cannot be null, empty or blank).
 *
 */
@XmlRootElement
@ApiModel(value="Person", description="Representation of a Person in the application.")
public class Person {

	@ApiModelProperty(value = "ID associated with the Person. It is unique for each Person.", readOnly=true)
	private String id;
	@ApiModelProperty(value = "Name associated with the Person.")
	private String name;
	@ApiModelProperty(value = "Country associated with the Person.")
	private String country;
	
	/**
	 * Empty constructor
	 */
	public Person() {}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	
	
}
